<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package gorirrajoe
 */

get_header();
?>

    <main id="main-content" class="main-content">

        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell">
                    <header class="blog-entry_header">
                        <h1><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'gorirrajoe' ); ?></h1>
                    </header><!-- .page-header -->
                </div>
            </div>

            <div class="grid-x grid-padding-x align-center">
                <div class="cell small-12 large-8">

                    <p><?php esc_html_e( 'It looks like nothing was found. Try a different search?', 'gorirrajoe' ); ?></p>

                    <div class="search-wrapper-404">
                        <?php
                        get_search_form();
                        ?>
                    </div>

                </div>

            </div><!-- .page-content -->
        </section><!-- .error-404 -->

    </main><!-- #main -->

<?php
get_footer();
