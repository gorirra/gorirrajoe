<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gorirrajoe
 */

get_header();
?>

<main id="main-content" class="main-content">

    <?php
        while (have_posts()) {
            the_post();

            if(get_post_type() === 'sketches') {
                get_template_part('template-parts/content', 'sketch');
            } else {
                get_template_part('template-parts/content', 'single');
            }

            /**
             * post navigation
             */
            $args = array(
                'prev_text' => '%title',
                'next_text' => '%title',
            );

            $previous = get_previous_post_link(
                '<div class="blog-entry_navigation-previous"><i class="icon icon-left-open"></i> %link</div>',
                $args['prev_text']
            );

            $next = get_next_post_link(
                '<div class="blog-entry_navigation-next text-right">%link <i class="icon icon-right-open"></i></div>',
                $args['next_text']
            );

            // Only add markup if there's somewhere to navigate to.
            if ( $previous || $next ) {
                echo '<div class="grid-container blog-entry_navigation">
                    <div class="grid-x grid-padding-x align-center">
                        <div class="cell small-11 large-10">
                            <div class="grid-x grid-padding-x blog-entry_navigation-tilt">
                                <div class="cell small-12 medium-6">'.
                                    $previous .'
                                </div>
                                <div class="cell small-12 medium-6">'.
                                    $next .'
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
            }
        }
    ?>
    <?php
        $current_category = get_the_category();
        $related_args = array(
            'numberposts' => 3,
            'exclude' => array(get_the_ID()),
            'fields' => 'ids',
            'orderby' => 'rand',
            'category' => $current_category[0]->term_id,
        );
        $related_posts = get_posts($related_args);
    ?>
    <?php if($related_posts): ?>
        <div class="grid-container blog-entry_related">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell small-11 large-10">
                    <h3 class="blog-entry_related-title">Related Posts</h3>
                </div>
            </div>
            <div class="grid-x grid-padding-x align-center">
                <div class="cell small-11 large-10">
                    <div class="grid-x grid-padding-x blog-entry_related-tilt">
                        <?php foreach($related_posts as $key => $related_id): ?>
                            <?php
                                if($key == 2) {
                                    $hide_class = 'hide-for-medium-only';
                                } else {
                                    $hide_class = '';
                                }
                            ?>
                            <div class="cell small-12 medium-6 large-4 <?php print $hide_class; ?>">
                                <div class="blog-entry_related-thumbnail-wrap">
                                    <?php
                                        $thumb_args = array(
                                            'postid' => $related_id,
                                            'img_size' => 'related-thumb',
                                        );
                                        gorirrajoe_post_thumbnail($thumb_args);
                                    ?>
                                </div>

                                <h4><a href="<?php print get_permalink($related_id); ?>"><?php print get_the_title($related_id); ?></a></h4>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</main><!-- #main -->

<?php
get_footer();
