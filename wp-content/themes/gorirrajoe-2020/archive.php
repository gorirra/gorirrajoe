<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gorirrajoe
 */

get_header();
?>

<main id="main-content" class="main-content">

    <?php if ( have_posts() ) { ?>
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell">
                    <header class="blog-entry_header">
                        <?php
                        the_archive_title( '<h1>', '</h1>' );
                        the_archive_description( '<p class="archive-description">', '</p>' );
                        ?>
                    </header><!-- .page-header -->
                </div>
            </div>
        </div>

        <?php
        /* Start the Loop */
        while ( have_posts() ) {
            the_post();

            /*
                * Include the Post-Type-specific template for the content.
                * If you want to override this in a child theme, then include a file
                * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                */
            get_template_part( 'template-parts/content', get_post_type() );

        }

        $newer = get_previous_posts_link(
            'Newer <i class="icon icon-right-open"></i>'
        );

        $older = get_next_posts_link(
            '<i class="icon icon-left-open"></i> Older'
        );

        if ( $older || $newer ) {
            echo '<div class="grid-container blog-entry_navigation">
                <div class="grid-x grid-padding-x align-center">
                    <div class="cell small-12 large-10">
                        <div class="grid-x grid-padding-x">
                            <div class="cell small-12 medium-6">
                                <div class="blog-entry_navigation-previous">'.
                                    $older .'
                                </div>
                            </div>
                            <div class="cell small-12 medium-6">
                                <div class="blog-entry_navigation-next text-right">'.
                                    $newer .'
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        }

    } else {

        get_template_part( 'template-parts/content', 'none' );

    }
    ?>

</main><!-- #main -->

<?php
get_footer();
