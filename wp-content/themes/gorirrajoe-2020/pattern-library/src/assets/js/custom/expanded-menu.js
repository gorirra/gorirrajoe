import $ from "jquery";

var $expandedMenuWrapper = $("#expandedMenuWrapper"),
    $closeMenuBottom = $("#toggleExpandedMenuBottom");

if ($expandedMenuWrapper.length) {
    var $toggleExpandedMenuButton = $("#toggleExpandedMenu");

    $toggleExpandedMenuButton.on("click", function () {
        $(this).toggleClass("button-active");
        $(this).find(".icon").toggleClass("hide");
        $("#toggleSearch").removeClass("button-active");

        if ($expandedMenuWrapper.hasClass("hide")) {
            $expandedMenuWrapper.removeClass("hide");
            $("#searchWrapper").removeClass("visible");

            setTimeout(function () {
                $("#searchWrapper").addClass("hide");
                $expandedMenuWrapper.addClass("visible");
            }, 50);
        } else {
            setTimeout(function () {
                $expandedMenuWrapper.addClass("hide");
            }, 250);

            $expandedMenuWrapper.removeClass("visible");
        }
    });

    $closeMenuBottom.on("click", function () {
        setTimeout(function () {
            $expandedMenuWrapper.addClass("hide");
        }, 250);

        $expandedMenuWrapper.removeClass("visible");
        $toggleExpandedMenuButton.toggleClass("button-active");
        $toggleExpandedMenuButton.find(".icon").toggleClass("hide");
    });
}
