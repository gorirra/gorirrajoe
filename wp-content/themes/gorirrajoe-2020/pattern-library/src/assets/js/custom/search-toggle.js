import $ from "jquery";

var $searchWrapper = $("#searchWrapper");

if ($searchWrapper.length) {
    var $toggleSearchButton = $("#toggleSearch"),
        $expandedMenuButton = $("#toggleExpandedMenu");

    $toggleSearchButton.on("click", function () {
        $(this).toggleClass("button-active");
        $("#toggleExpandedMenu").removeClass("button-active");

        if ($searchWrapper.hasClass("hide")) {
            $searchWrapper.removeClass("hide");
            $("#expandedMenuWrapper").removeClass("visible");

            setTimeout(function () {
                $("#expandedMenuWrapper").addClass("hide");
                $expandedMenuButton.find('.icon-menu').removeClass('hide');
                $expandedMenuButton.find('.icon-cancel').addClass('hide');
                $searchWrapper.addClass("visible");
                $("#searchInput").focus();
            }, 250);
        } else {
            setTimeout(function () {
                $searchWrapper.addClass("hide");
            }, 250);

            $searchWrapper.removeClass("visible");
        }
    });
}
