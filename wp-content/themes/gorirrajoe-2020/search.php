<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package gorirrajoe
 */
global $wp_query;
get_header();
?>

    <main id="main-content" class="main-content">

        <?php if ( have_posts() ) { ?>
            <div class="grid-container">
                <div class="grid-x grid-padding-x align-center">
                    <div class="cell">
                        <header class="blog-entry_header">
                            <h1>
                                <?php
                                /* translators: %s: search query. */
                                printf( esc_html__( '%d Results Found for: &ldquo;%s&rdquo;', 'gorirrajoe' ), $wp_query->found_posts, '<span>' . get_search_query() . '</span>' );
                                ?>
                            </h1>
                        </header>
                    </div>
                </div>
            </div>


            <?php
            /* Start the Loop */
            while ( have_posts() ) {
                the_post();

                /**
                 * Run the loop for the search to output the results.
                 * If you want to overload this in a child theme then include a file
                 * called content-search.php and that will be used instead.
                 */
                get_template_part( 'template-parts/content', 'post' );

            }

            $newer = get_previous_posts_link(
                'Newer <i class="icon icon-right-open"></i>'
            );

            $older = get_next_posts_link(
                '<i class="icon icon-left-open"></i> Older'
            );

            if ( $older || $newer ) {
                echo '<div class="grid-container blog-entry_navigation">
                    <div class="grid-x grid-padding-x align-center">
                        <div class="cell small-12 large-10">
                            <div class="grid-x grid-padding-x">
                                <div class="cell small-12 medium-6">
                                    <div class="blog-entry_navigation-previous">'.
                                        $older .'
                                    </div>
                                </div>
                                <div class="cell small-12 medium-6">
                                    <div class="blog-entry_navigation-next text-right">'.
                                        $newer .'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
            }

        } else {

            get_template_part( 'template-parts/content', 'none' );

        }
        ?>

    </main><!-- #main -->

<?php
get_footer();
