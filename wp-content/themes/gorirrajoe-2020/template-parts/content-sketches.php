<?php
/**
 * Template part for displaying sketches (blogroll)
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gorirrajoe
 */

$image_id  = get_post_thumbnail_id();
$image_url = wp_get_attachment_image_src($image_id, 'large');
$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', 1);
?>
<article <?php post_class('cell medium-6 large-4 blog-entry sketches');?>>
    <a href="<?php echo esc_url(get_permalink()); ?>" class="sketches_link">
        <div class="sketches_polygon-wrapper">
            <div role="img" aria-label="<?php echo $image_alt; ?>" class="sketches_polygon lozad" data-background-image="<?php echo $image_url[0]; ?>"></div>
        </div>
        <?php the_title('<h2 class="sketches_title h4">', '</h2>');?>
    </a>
</article>
