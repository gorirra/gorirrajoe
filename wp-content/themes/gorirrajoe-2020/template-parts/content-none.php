<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gorirrajoe
 */

?>

<div class="grid-container">
    <div class="grid-x grid-padding-x align-center">
        <div class="cell">
            <header class="blog-entry_header">
                <h1><?php esc_html_e( 'Nothing Found', 'gorirrajoe' ); ?></h1>
            </header>
        </div>
    </div>

    <div class="grid-x grid-padding-x align-center">
        <div class="cell small-12 large-8">
            <?php
            if ( is_search() ) { ?>

                <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'gorirrajoe' ); ?></p>

                <div class="search-wrapper-404">
                    <?php
                    get_search_form();
                    ?>
                </div>

            <?php } else { ?>

                <p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'gorirrajoe' ); ?></p>

                <div class="search-wrapper-404">
                    <?php
                    get_search_form();
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
