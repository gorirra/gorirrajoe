<?php
/**
 * Template part for displaying sketches (blogroll)
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gorirrajoe
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('blog-entry'); ?>>
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-8">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</article>
