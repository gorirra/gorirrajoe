<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gorirrajoe
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('blog-entry'); ?>>
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <header class="blog-entry_header">
                    <div class="grid-x grid-padding-x align-middle">

                        <div class="cell">
                            <?php
                                the_title( '<h1>', '</h1>' );
                            ?>
                        </div>

                    </div>
                </header>
            </div>
        </div>

        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-8">
                <div class="blog-entry_content">
                    <?php
                    the_content();

                    $image_id  = get_post_thumbnail_id();

                    echo wp_get_attachment_image($image_id, 'large');
                    ?>
                </div>
            </div>
        </div>
    </div>
</article>
