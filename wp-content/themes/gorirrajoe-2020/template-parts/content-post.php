<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gorirrajoe
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('blog-entry'); ?>>
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <header class="blog-entry_header">
                    <div class="grid-x grid-padding-x align-middle">

                        <div class="cell shrink">
                            <?php gorirrajoe_posted_on(); ?>
                        </div>

                        <div class="cell auto">
                            <?php
                            if(is_archive()) {
                                the_title( '<h3 class="blog-entry_header-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
                            } else {
                                the_title( '<h2 class="blog-entry_header-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                            }
                            ?>
                        </div>

                    </div>
                </header>
            </div>
        </div>

        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-8">
                <div class="grid-x grid-padding-x align-center">

                    <?php if(has_post_thumbnail()) { ?>
                        <div class="small-8 medium-4 cell">
                            <div class="blog-entry_thumbnail-wrap">
                                <?php gorirrajoe_post_thumbnail(); ?>
                            </div>
                        </div>
                        <div class="medium-8 cell">
                            <section class="blog-entry_summary">
                                <?php
                                echo gorirrajoe_custom_the_content(get_the_content('Read more &raquo;'));
                                ?>
                            </section>
                            <footer class="blog-entry_footer">
                                <?php gorirrajoe_entry_footer(); ?>
                            </footer>
                        </div>
                    <?php } else { ?>
                        <div class="medium-12 cell">
                            <section class="blog-entry_summary">
                                <?php
                                echo gorirrajoe_custom_the_content(get_the_content('Read more &raquo;'));
                                ?>
                            </section>
                            <footer class="blog-entry_footer">
                                <?php gorirrajoe_entry_footer(); ?>
                            </footer>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</article>
