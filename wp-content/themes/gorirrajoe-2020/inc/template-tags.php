<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package gorirrajoe
 */

if (!function_exists('gorirrajoe_posted_on')) {
    /**
     * Prints HTML with meta information for the current post-date/time.
     */
    function gorirrajoe_posted_on()
    {
        $time_string = '<time class="blog-entry_date" datetime="%1$s">
            <div class="blog-entry_month-day">%2$s</div>
            <div class="blog-entry_year">%3$s</div>
        </time>';

        $time_string = sprintf(
            $time_string,
            esc_attr(get_the_date(DATE_W3C)),
            esc_html(get_the_date('M d')),
            esc_html(get_the_date('Y'))
        );

        echo $time_string; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

    }
}

if (!function_exists('gorirrajoe_entry_footer')) {
    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function gorirrajoe_entry_footer()
    {
        // Hide category and tag text for pages.
        if ('post' === get_post_type()) {
            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list(esc_html__(', ', 'gorirrajoe'));

            if ($categories_list) {
                /* translators: 1: list of categories. */
                printf('<span class="cat-links">' . esc_html__('Posted in %1$s', 'gorirrajoe') . '</span>', $categories_list); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            }
        }
    }
}

if (!function_exists('gorirrajoe_post_thumbnail')) {
    /**
     * Displays an optional post thumbnail.
     */
    function gorirrajoe_post_thumbnail($args = null)
    {
        $defaults = array(
            'postid' => get_the_ID(),
            'img_size' => 'main-thumb',
        );
        $args = wp_parse_args($args, $defaults); ?>

        <?php $default_thumb = gorirrajoe_get_option('default_thumbnail_id'); ?>

        <a href="<?php print get_the_permalink($args['postid']);?>">
            <?php
                $img = get_the_post_thumbnail_url($args['postid'], $args['img_size']);

                if($img) {
                    $img_alt = get_post_meta(get_post_thumbnail_id($args['postid']), '_wp_attachment_image_alt', 1);
                } else {
                    $img_array = wp_get_attachment_image_src($default_thumb, $args['img_size']);
                    $img = $img_array[0];
                    $img_alt = get_post_meta($default_thumb, '_wp_attachment_image_alt', 1);
                }
            ?>
            <img class="lozad blog-entry_thumbnail" data-src="<?php echo $img; ?>" alt="<?php echo $img_alt; ?>">
        </a>

    <?php }
}

if (!function_exists('wp_body_open')) {
    /**
     * Shim for sites older than 5.2.
     *
     * @link https://core.trac.wordpress.org/ticket/12563
     */
    function wp_body_open()
    {
        do_action('wp_body_open');
    }
}
