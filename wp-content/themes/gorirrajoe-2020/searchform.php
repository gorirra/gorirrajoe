<form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
    <div class="grid-x grid-padding-x align-middle">
        <div class="cell shrink">
            <label>Search</label>
        </div>
        <div class="cell auto">
            <input id="searchInput" type="search" class="search-field" placeholder="<?php echo esc_attr_x('Whatcha lookin\' for?', 'placeholder') ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label') ?>" />
        </div>
        <div class="cell shrink align-self-stretch">
            <input type="submit" class="button button-submit" value="<?php echo esc_attr_x('Search', 'submit button') ?>" />
        </div>
    </div>
</form>
