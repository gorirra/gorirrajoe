<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gorirrajoe
 */

?>
 <footer id="footer" class="footer">
    <div class="grid-container">
        <div class="grid-x grid-padding-x text-center">
            <div class="cell auto">
                <p>&copy; <?php echo date('Y'); ?> &mdash; <?php bloginfo('name');?></p>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
