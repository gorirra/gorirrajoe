<?php
/*
* Template Name: Resume
*/

$id = get_the_ID();
$resume         = get_post_meta($id, '_resumee', 1);
$city_state_zip = get_post_meta($id, '_city_state_zip', 1);
$phone          = get_post_meta($id, '_phone', 1);
$email          = get_post_meta($id, '_email', 1);

get_header('resumee');
?>
<main id="main-content" class="main-content">
    <?php
    while ( have_posts() ) {
        the_post();

        get_template_part( 'template-parts/content', 'resumee' );
    }
    ?>
</main>
<?php
get_footer();
