<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gorirrajoe
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head();?>
</head>

<body <?php body_class();?>>
    <?php wp_body_open();?>

    <header id="masthead" class="header">
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-justify align-middle">

                <div class="cell shrink">
                    <?php
                    if ( is_front_page() && is_home() ) {
                        echo '<h1 class="header_title"><a href="'. esc_url(home_url('/')) .'" rel="home">'. get_bloginfo('name') .'</a></h1>';
                    } else {
                        echo '<h2 class="header_title"><a href="'. esc_url(home_url('/')) .'" rel="home">'. get_bloginfo('name') .'</a></h2>';
                    }
                    ?>
                </div>

                <div class="cell auto text-right">
                    <ul class="no-bullet margin-none list-flex align-right">
                        <li class="show-for-large"><a href="<?php echo gorirrajoe_get_option('twitter_url'); ?>"><i class="icon-twitter"></i></a></li>
                        <li class="show-for-large"><a href="<?php echo gorirrajoe_get_option('instagram_url'); ?>"><i class="icon-instagram"></i></a></li>
                        <li class="show-for-large"><a href="<?php echo gorirrajoe_get_option('linkedin_url'); ?>"><i class="icon-linkedin-squared"></i></a></li>
                        <li class="show-for-large">
                            <button id="toggleSearch" type="button" class="button-search">
                                <i class="icon-search"><span class="show-for-sr">Search</span></i>
                            </button>
                        </li>
                        <li>
                            <button id="toggleExpandedMenu" type="button" class="button-menu">
                                <i class="icon icon-menu"><span class="show-for-sr">Open menu</span></i>
                                <i class="icon icon-cancel hide"><span class="show-for-sr">Close menu</span></i>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <div id="searchWrapper" class="search-wrapper hide" style="max-height: 0;">
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-right">

                <div class="cell medium-6">
                    <?php get_search_form();?>
                </div>

            </div>
        </div>
    </div>

    <div id="expandedMenuWrapper" class="expanded-menu-wrapper hide" style="max-height: 0;">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell medium-4">

                    <div class="expanded-menu_block">
                        <p class="h4">Pages</p>
                        <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'pages',
                                    'menu_class'     => 'expanded-menu_list',
                                )
                            );
                        ?>
                    </div>

                </div>
                <div class="cell medium-4">
                    <div class="expanded-menu_block">
                        <p class="h4">Archives</p>

                        <div class="grid-x grid-padding-x">
                            <?php
                                if (false === ($archives_markup = get_transient('archives_markup_results'))) {
                                    $archives_markup = get_posts_years_markup();
                                    set_transient('archives_markup_results', $archives_markup, TRANSIENT);
                                }

                                echo $archives_markup;
                            ?>
                        </div>

                    </div>
                </div>
                <div class="cell medium-4">
                    <div class="expanded-menu_block">
                        <p class="h4">Categories</p>
                        <div class="grid-x grid-padding-x">
                            <?php
                                if (false === ($categories_markup = get_transient('categories_markup_results'))) {
                                    $categories_markup = get_categories_markup();
                                    set_transient('categories_markup_results', $categories_markup, TRANSIENT);
                                }

                                echo $categories_markup;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-container hide-for-medium">
            <div class="grid-x grid-padding-x align-right">
                <div class="cell small-shrink text-right">
                    <button id="toggleExpandedMenuBottom" type="button" class="button-menu-bottom">
                        <i class="icon icon-cancel"><span class="show-for-sr">Close menu</span></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
