<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gorirrajoe
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head();?>
</head>

<body <?php body_class('resume');?>>
    <?php wp_body_open();?>

    <header id="masthead" class="header">
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-justify align-middle">

                <div class="cell shrink">
                    <h1 class="header_title header_title-resume"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home">Joseph Hernandez</a></h1>
                </div>
            </div>
        </div>
    </header>
