<?php
/**
 * gorirrajoe functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gorirrajoe
 */

if (!function_exists('gorirrajoe_setup')) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function gorirrajoe_setup()
	{
		/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on gorirrajoe, use a find and replace
		* to change 'gorirrajoe' to the name of your theme in all the template files.
		*/
		load_theme_textdomain('gorirrajoe', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support('title-tag');

		/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
		add_theme_support('post-thumbnails');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'pages' => esc_html__('Pages', 'gorirrajoe'),
			)
		);

		/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		add_image_size('main-thumb', 580);
		add_image_size('related-thumb', 580, 580, true);
	}
}
add_action('after_setup_theme', 'gorirrajoe_setup');

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gorirrajoe_widgets_init()
{
	register_sidebar(
		array(
			'name'          => esc_html__('Sidebar', 'gorirrajoe'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'gorirrajoe'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'gorirrajoe_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function gorirrajoe_scripts()
{
	wp_dequeue_style('wp-block-library');
	wp_dequeue_style('wp-block-library-theme');
	wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Fanwood+Text:ital@0;1&family=Inconsolata&family=Shrikhand&display=swap', array(), null);

	wp_enqueue_style('gorirrajoe-style', get_template_directory_uri() . '/dist/assets/css/app.css', array(), filemtime(get_template_directory() . '/dist/assets/css/app.css'));

	wp_deregister_script('jquery');
	wp_deregister_script('wp-embed');
	wp_enqueue_script('gorirrajoe-script', get_template_directory_uri() . '/dist/assets/js/app.js', array(), filemtime(get_template_directory() . '/dist/assets/js/app.js'), true);
}
add_action('wp_enqueue_scripts', 'gorirrajoe_scripts');

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * remove generator and emoji head tags
 */
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

// Add a11y skip link
function gorirrajoe_add_a11y_skip_link()
{
	echo '<a class="bypass-block show-on-focus" href="#main-content">' . esc_attr_x('Skip to content', 'gorirrajoe') . '</a>';
}
add_action('wp_body_open', 'gorirrajoe_add_a11y_skip_link');

// get years with at least one published post
function get_posts_years_markup()
{
	$years_markup = '';

	$years = wp_get_archives(
		array(
			'type' => 'yearly',
			'echo' => false,
		)
	);

	$years_array = explode("\t", $years);

	$years_markup .= '<div class="cell small-12">
		<ul class="expanded-menu_list">';

			foreach($years_array as $key => $value) {
				$years_markup .= $value;
			}

		$years_markup .= '</ul>
	</div>';

	return $years_markup;
}

function get_categories_markup()
{
	$category_markup = '';

	$categories_array = get_categories(
		array(
			'hide_empty' => true,
			'parent'     => 0,
		)
	);

	$category_markup .= '<div class="cell small-12">
		<ul class="expanded-menu_list">';

			foreach($categories_array as $key => $value) {
				$category_markup .= '<li><a href="' . get_site_url(null, '/' . $value->taxonomy . '/' . $value->slug) . '">' . $value->name . '</a></li>';
			}

		$category_markup .= '</ul>
	</div>';

	return $category_markup;
}

function gorirrajoe_custom_the_content($content)
{
	$content = str_replace('h2', 'h3', $content);
	return apply_filters('the_content', $content);
}

/**
 * Preconnect to Google Fonts URL WP native way
 *
 * @param array  $urls          URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for, e.g. 'preconnect' or 'prerender'.
 *
 * @return array
 */
function preconnect_google_fonts($urls, $relation_type)
{
	if ('preconnect' === $relation_type) {
		$urls[] = 'https://fonts.gstatic.com';
		$urls[] = 'https://www.google-analytics.com';
	}

	return $urls;
}
add_filter('wp_resource_hints', 'preconnect_google_fonts', 10, 2);

/**
 * web perf: preload fonts
 */
function gorirrajoe_preload_fonts()
{
	echo '<link rel="preload" as="style" href="https://fonts.googleapis.com/css2?family=Fanwood+Text:ital@0;1&family=Inconsolata&family=Shrikhand&display=swap" />' . "\n";
	echo '<link rel="preload" href="' . get_template_directory_uri() . '/dist/assets/font/fontello.woff2?54782262" as="font" type="font/woff2" crossorigin>' . "\n";
}
add_action('wp_head', 'gorirrajoe_preload_fonts', 0);

/**
 * add favicons to head
 */
function gorirrajoe_add_favicon()
{
	echo '<link rel="apple-touch-icon" sizes="180x180" href="' . get_template_directory_uri() . '/dist/assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="' . get_template_directory_uri() . '/dist/assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="' . get_template_directory_uri() . '/dist/assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="' . get_template_directory_uri() . '/dist/assets/img/favicon/site.webmanifest">
	<link rel="mask-icon" href="' . get_template_directory_uri() . '/dist/assets/img/favicon/safari-pinned-tab.svg" color="#ff3b3f">
	<meta name="msapplication-TileColor" content="#603cba">
	<meta name="theme-color" content="#ffffff">' . "\n";
}
add_action('wp_head', 'gorirrajoe_add_favicon');

/**
 * manually add analytics
 * not using for now -- testing out site kit plugin
 */
function gorirrajoe_add_analytics()
{?>
	<!-- Google Analytics -->
	<script>
	window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
	ga('create', 'UA-88834-5', 'auto');
	ga('send', 'pageview');
	</script>
	<script async src='https://www.google-analytics.com/analytics.js'></script>
	<!-- End Google Analytics -->
<?php }
// add_action('wp_head', 'gorirrajoe_add_analytics', 3);

/**
 * change up markup for images
 */
function gorirrajoe_image_block($block_content, $block)
{
	if ('core/image' === $block['blockName']) {
		$img       = wp_get_attachment_image_src($block['attrs']['id'], $block['attrs']['sizeSlug']);
		$img_alt   = get_post_meta($block['attrs']['id'], '_wp_attachment_image_alt', 1);
		$img_align = array_key_exists('align', $block['attrs']) ? 'float-' . $block['attrs']['align'] : '';

		$regex = '#<\s*?figcaption\b[^>]*>(.*?)</figcaption\b[^>]*>#s';
		preg_match($regex, $block['innerContent'][0], $img_caption_array);

		$img_caption = !empty($img_caption_array) ? '<figcaption>' . $img_caption_array[1] . '</figcaption>' : '';

		$block_content = '<figure class="wp-block-image ' . $img_align . '">
			<img data-src="' . $img[0] . '" alt="' . $img_alt . '" class="lozad">' .
			$img_caption . '
		</figure>';
	}
	return $block_content;
}
add_filter('render_block', 'gorirrajoe_image_block', 10, 2);
