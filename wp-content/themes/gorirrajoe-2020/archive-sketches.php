<?php
/**
 * Template Name: Sketches
 *
 * @package gorirrajoe
 */

get_header();
?>
<main id="main-content" class="main-content">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell">
                <header class="blog-entry_header">
                    <h1>
                        <?php
                        the_archive_title( '<h1>', '</h1>' );
                        ?>
                    </h1>
                </header><!-- .page-header -->
            </div>
        </div>

        <div class="grid-x grid-margin-x align-center">
            <?php
            while ( have_posts() ) {
                the_post();

                get_template_part( 'template-parts/content', 'sketches' );
            }
            ?>
        </div>
    </div>

    <?php
    $newer = get_previous_posts_link(
        'Newer <i class="icon icon-right-open"></i>'
    );

    $older = get_next_posts_link(
        '<i class="icon icon-left-open"></i> Older'
    );

    if ( $older || $newer ) {
        echo '<div class="grid-container blog-entry_navigation">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell small-12 large-10">
                    <div class="grid-x grid-padding-x">
                        <div class="cell small-12 medium-6">
                            <div class="blog-entry_navigation-previous">'.
                                $older .'
                            </div>
                        </div>
                        <div class="cell small-12 medium-6">
                            <div class="blog-entry_navigation-next text-right">'.
                                $newer .'
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
    }
    ?>
</main><!-- #main -->
<?php
get_footer();
