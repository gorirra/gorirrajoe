<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
/*
Template Name: Resume
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
  <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
  <title><?php wp_title('|', true, 'right'); ?> <?php bloginfo('name'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
  <?php wp_head(); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
</head>
  <body <?php body_class(); ?>>
    <div id="container">
      <div id="banner">
        <div id="nameAndLoc">
          <h3 id="bigName">Joseph Hernandez</h3>
          <ul class="location">
            <li>San Diego, CA 92127</li>
            <li>858.245.8573</li>
            <li><a href="mailto:joeyhernandez@gmail.com">joeyhernandez@gmail.com</a></li>
          </ul>
        </div>
        <div class="jig"></div>
      </div>
      <div id="contentResume" role="main">
        <?php if (have_posts()) :while (have_posts()) :the_post(); ?>
        <div class="post" id="post-<?php the_ID(); ?>">
          <div class="entry">
            <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
            <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
          </div>
        </div>
        <?php endwhile; endif; ?>
        <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
      </div>
      <div id="sidebar2">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('resumeSidebar') ) : ?>
        <?php endif; ?>
      </div>
      <div id="footer" role="contentinfo">
        &copy; <?php echo date('Y'); ?> - <?php bloginfo('name'); ?>
      </div>
    </div>
    <?php wp_footer(); ?>
  </body>
</html>
