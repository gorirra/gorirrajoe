<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
  <div id="sidebar" role="complementary">
    <ul>
      <li class="widget">
        <h3>Pages</h3>
        <?php wp_nav_menu(array('container' => false)); ?>
      </li>
      <?php   /* Widgetized sidebar, if you have the plugin installed. */
          if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>
      <?php endif; ?>
    </ul>
    <div class="jig"></div>
  </div>

