<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/javascript/sort_tables_by_columns.js"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/sort_tables_by_columns.css" type="text/css" media="screen" />

  <div id="content" role="main">
    <?php if (have_posts()) :while (have_posts()) :the_post(); ?>
    <div class="post" id="post-<?php the_ID(); ?>">
    <h1><?php the_title(); ?></h1>
      <div class="entry">
        <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
        <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
      </div>
    </div>
    <?php endwhile; endif; ?>
  <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
  </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
