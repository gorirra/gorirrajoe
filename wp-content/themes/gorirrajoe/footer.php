<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
      <div class="jig"></div>
      <div id="footer" role="contentinfo">
        &copy; <?php echo date('Y'); ?> - <?php bloginfo('name'); ?>
      </div>
    </div>
    <?php wp_footer(); ?>
  </body>
</html>
