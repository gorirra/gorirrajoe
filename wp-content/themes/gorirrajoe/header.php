<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('|', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
  <div id="container">
    <div id="banner">
      <h3 id="logo"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h3>
      <ul id="socNetSearch">
        <li><a href="http://twitter.com/gorirrajoe" target="_blank"><img class="socnet twitter" src="<?php bloginfo('stylesheet_directory'); ?>/images/spacer.gif" alt="Follow me on Twitter" title="Follow me on Twitter" /></a></li>
        <li><a href="https://www.facebook.com/gorirrajoe" target="_blank"><img class="socnet facebook" src="<?php bloginfo('stylesheet_directory'); ?>/images/spacer.gif" alt="Friend me on Facebook" title="Friend me on Facebook" /></a></li>
        <li><a href="http://instagram.com/gorirrajoe" target="_blank"><img class="socnet instagram" src="<?php bloginfo('stylesheet_directory'); ?>/images/spacer.gif" alt="Follow me on Instagram" title="Follow me on Instagram" /></a></li>
        <li><a href="http://www.linkedin.com/pub/joey-hernandez/2/435/1bb" target="_blank"><img class="socnet linkedin" src="<?php bloginfo('stylesheet_directory'); ?>/images/spacer.gif" alt="Connect with me on Linkedin" title="Connect with me on Linkedin" /></a></li>
        <li><a href="https://foursquare.com/gorirrajoe" target="_blank"><img class="socnet foursquare" src="<?php bloginfo('stylesheet_directory'); ?>/images/spacer.gif" alt="Stalk me on Foursquare" title="Stalk me on Foursquare" /></a></li>
        <li><a href="https://plus.google.com/116485937691144314000/posts?hl=en" target="_blank"><img class="socnet gplus" src="<?php bloginfo('stylesheet_directory'); ?>/images/spacer.gif" alt="Circle me on Google+" title="Circle me on Google+" /></a></li>
        <li class="searchBar"><?php get_search_form2(); ?></li>
      </ul>
      <div class="jig"></div>
    </div>
    <?php if ( is_home() | is_404() ) { ?>
      <div id="photoContainer">
        <?php echo do_shortcode( '[responsive_slider]' ); ?> 
      </div>
    <?php } ?>

