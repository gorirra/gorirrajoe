<?php
/* Template Name: Sketches Template */

get_header(); ?>
<div id="content" role="main">

	<?php query_posts(
		array(
			'post_type'			=> 'sketches',
			'posts_per_page'	=> -1
		)
	); ?>

	<h1><?php the_title(); ?></h1>

	<div class="post" id="post-<?php the_ID(); ?>">
		<ol class="sketch_list">
			<?php
				if ( have_posts() ) : while ( have_posts() ) : the_post();
					$large_image_url = wp_get_attachment_image_src(  get_post_thumbnail_id(), 'large' ); ?>

					<li>
						<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
							<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/spacer.gif" class="single_sketch" style="background:url( '<?php echo $large_image_url[0]; ?>' ) 50% 50% no-repeat #fff;" />
						</a>
						<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					</li>

				<?php endwhile; endif;

				wp_reset_query();
			?>
		</ol>
		<div class="jig"></div>
	</div>

</div>


<?php
	get_sidebar();
	get_footer();
?>
