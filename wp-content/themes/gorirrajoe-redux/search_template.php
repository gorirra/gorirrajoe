<?php
/* Template Name: Search Template */

get_header(); ?>
  <div id="content" role="main">
    <h1><?php the_title(); ?></h1>
    <div class="post" id="post-<?php the_ID(); ?>">
      <?php get_search_form(); ?>
      <div class="jig"></div>
    </div>
  <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
  </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
