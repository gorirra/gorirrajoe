<?php
/**
* Custom Metaboxes | Portfolio
*/
class GorirraJoe_Metadata_Portfolio {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function gorirrajoe_metadata_portfolio() {
		$prefix = '_gj_';

		/**
		* Initiate the metabox
		*/
		$cmb = new_cmb2_box( array(
			'id'           => 'portfolio',
			'title'        => __( 'Portfolio Options', 'vn' ),
			'object_types' => array( 'portfolio', ), // Post type
			'context'      => 'normal',
			'priority'     => 'high',
			'show_names'   => true // Show field names on the left
		) );

		$cmb->add_field( array(
			'name' => 'Project Screenshots',
			'id'   => $prefix . 'screenshots',
			'type' => 'file_list',
		) );

		$cmb->add_field( array(
			'name' => 'Straight to External URL',
			'id'   => $prefix . 'url',
			'desc' => 'if not empty, link thumbnail will link directly to provided url (bitbucket)',
			'type' => 'text_url',
		) );

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'gorirrajoe_metadata_portfolio' ) );
	}
}
