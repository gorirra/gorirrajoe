<?php
/**
 * Custom Post Types | Portfolio
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */

class GorirraJoe_CPT_Portfolio {

	static $instance	= false;

	public function __construct() {

		$this->gorirrajoe_posttype_portfolio();

	}

	/**
	 * Register Meta Boxes
	 *
	 * Defines all the meta boxes with CMB2 used by this custom post type.
	 *
	 */
	protected function gorirrajoe_posttype_portfolio() {

		$labels = array(
			'name'					=> _x( 'Portfolio Items', 'post type general name' ),
			'singular_name'			=> _x( 'Portfolio Item', 'post type singular name' ),
			'add_new'				=> _x( 'Add New Portfolio Item', 'Sweet' ),
			'add_new_item'			=> __( 'Add New Portfolio Item' ),
			'edit_item'				=> __( 'Edit Portfolio Item' ),
			'new_item'				=> __( 'New Portfolio Item' ),
			'view_item'				=> __( 'View Portfolio Items' ),
			'search_items'			=> __( 'Search Portfolio Items' ),
			'not_found'				=>  __( 'No Portfolio Items found' ),
			'not_found_in_trash'	=> __( 'No Portfolio Items found in Trash' ),
			'parent_item_colon'		=> ''
		 );

		$supports = array( 'title', 'editor', 'revisions', 'thumbnail', 'comments' );

		register_post_type( 'portfolio',
			array(
				'labels'	=> $labels,
				'public'	=> true,
				'supports'	=> $supports,
				'menu_icon' => 'dashicons-admin-appearance',
			 )
		);
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}

}
