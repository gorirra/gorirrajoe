<?php
	if( get_post_type() === 'portfolio' ) {

		get_header( 'resumee' );

	} else {

		get_header();

	}

	if( in_category( 'photography' ) ) {
		$pix = apply_filters( 'the_content', get_post_meta( get_the_ID(), '_pix', true ) ); ?>

		<div id="photo_content" role="main">

			<?php if ( get_adjacent_post( false, '', false ) != '' ) { ?>

				<div class="navigation">
					<div><?php next_post_link( '%link' ); ?></div>
				</div>

			<?php }


			if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<h1><?php the_title(); ?></h1>

					<div class="postdate"><?php the_time( 'F jS, Y' ) ?></div>

					<div class="entry">
						<div class="intro">
							<?php the_content( '<p class="serif">Read the rest of this entry &raquo;</p>' ); ?>
						</div>

						<div class="gallery_section">
							<?php echo $pix; ?>
						</div>

						<?php
							wp_link_pages( array( 'before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number' ) );
							the_tags( '<p class="metadata">Tags: ', ', ', '</p>' );
						?>
					</div>
				</div>


				<?php comments_template();

				if ( get_adjacent_post( false, '', true ) != '' ) { ?>

					<div class="navigation">
						<div><?php previous_post_link( '%link' ); ?></div>
					</div>

				<?php }

			endwhile; else: ?>

				<p>Sorry, no posts matched your criteria.</p>

			<?php endif;

		get_sidebar(); ?>

		</div>

	<?php } elseif( get_post_type() === 'portfolio' ) {

		$page			= get_page_by_path( 'resume' );
		$resume_pageid	= $page->ID;

		$resume			= get_post_meta( $resume_pageid, '_resumee', 1 );
		$city_state_zip	= get_post_meta( $resume_pageid, '_city_state_zip', 1 );
		$phone			= get_post_meta( $resume_pageid, '_phone', 1 );
		$email			= get_post_meta( $resume_pageid, '_email', 1 ); ?>

		<div id="banner" class="banner">

			<div class="banner__back">
				<a href="<?php echo site_url( '/resume/' ); ?>">Back to Portfolio</a>
			</div>

			<div id="nameAndLoc">
				<h3 id="bigName">Joseph Hernandez</h3>
				<ul class="location">
					<li><?php echo $city_state_zip; ?></li>
					<li><?php echo $phone; ?></li>
					<li><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></li>

					<?php if( $resume != '' ) {
						echo '<li><a href="'. $resume .'" target="_blank"><img class="download_pdf" src="'. get_bloginfo( 'stylesheet_directory' ) .'/images/pdf_icon.gif" alt="Download Résumé as PDF">Download Résumé as PDF</a></li>';
					} ?>

				</ul>
			</div>

		</div>

		<div id="contentResume" role="main" class="portfolio">

			<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

				<div class="post" id="post-<?php the_ID(); ?>">

					<div class="entry">
						<?php
							echo '<h1>'. get_the_title() .'</h1>';

							the_content();


							/**
							 * screenshot carousel
							 */
							$screenshots = get_post_meta( get_the_ID(), '_gj_screenshots', 1 ) != '' ? get_post_meta( get_the_ID(), '_gj_screenshots', 1 ) : '';

							if( $screenshots != '' ) {
								echo '<div class="owl-carousel owl-theme">';

									foreach( $screenshots as $attachment_id => $attachment_url ) {

										$img_array = wp_get_attachment_image_src( $attachment_id, 'full' );

										echo '<a href="'. $img_array[0] .'" data-fancybox="images"><img class="owl-lazy" data-src="'. $img_array[0] .'"></a>';

									}

								echo '</div>';

							}
						?>
					</div>

				</div>

			<?php
				endwhile; endif;


				/**
				 * portfolio content
				 */
				if ( false === ( $portfolio_query = get_transient( 'portfolio_query_results' ) ) ) {
					$args = array(
						'posts_per_page'	=> -1,
						'post_type'			=> 'portfolio',
					);
					$portfolio_query = new WP_Query( $args );
					set_transient( 'portfolio_query_results', $portfolio_query, 60 * MINUTE_IN_SECONDS );
				}

				if( $portfolio_query->have_posts() ) {

					echo '<h2>More Projects</h2>
					<ul class="portfolio">';

						while( $portfolio_query->have_posts() ) {
							$portfolio_query->the_post();

							echo '<li class="portfolio__item">
								<a href="'. get_the_permalink() .'">
									<img src="'. get_the_post_thumbnail_url( get_the_ID(), 'medium' ) .'" alt="'. get_the_title() .'">
									<p>'. get_the_title() .'</p>
								</a>
							</li>';

						}

					echo '</ul>';

				}
			?>

		</div>

	<?php } else { ?>

		<div id="content" role="main">

			<?php if ( get_adjacent_post( false, '', false ) != '' ) { ?>

				<div class="navigation">
					<div><?php next_post_link( '%link' ); ?></div>
				</div>

			<?php }

			if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<h1><?php the_title(); ?></h1>

					<div class="postdate"><?php the_time( 'F jS, Y' ) ?></div>

					<div class="entry">
						<?php
							the_content( '<p class="serif">Read the rest of this entry &raquo;</p>' );
							wp_link_pages( array( 'before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number' ) );
							the_tags( '<p class="metadata">Tags: ', ', ', '</p>' );
						?>
					</div>
				</div>

				<?php comments_template();

				if ( get_adjacent_post( false, '', true ) != '' ) { ?>

					<div class="navigation">
						<div><?php previous_post_link( '%link' ) ?></div>
					</div>

				<?php }

			endwhile; else: ?>

				<p>Sorry, no posts matched your criteria.</p>

			<?php endif; ?>

		</div>

		<?php get_sidebar();
	}

	get_footer();
?>
