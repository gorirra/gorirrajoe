<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<title><?php wp_title('|', true, 'right'); ?> <?php bloginfo('name'); ?></title>

		<?php wp_head(); ?>

	</head>


	<body <?php body_class(); ?>>

		<div id="top"></div>

		<div id="container">
			<div id="banner">
				<div class="expand">
					<a href="#menu"><span class="icon-down-open"></span></a>
				</div>

				<h3 id="logo"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h3>

				<ul id="socNetSearch">
					<li><a href="http://twitter.com/gorirrajoe" target="_blank"><span class="icon-twitter"></span></a></li>
					<li><a href="http://instagram.com/gorirrajoe" target="_blank"><span class="icon-instagram"></span></a></li>
					<li><a href="http://www.linkedin.com/pub/joey-hernandez/2/435/1bb" target="_blank"><span class="icon-linkedin-squared"></span></a></li>
					<li><a href="#" id="search_toggle"><span class="icon-search"></span></a></li>
				</ul>

				<div class="hidden_search">
					<?php include (TEMPLATEPATH . '/searchform.php'); ?>
				</div>

			</div>
