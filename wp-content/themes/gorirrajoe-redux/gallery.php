<?php
/* Template Name: Gallery */

$pix = apply_filters('the_content', get_post_meta( get_the_ID(), '_pix', true ));
get_header(); ?>
  <div id="full_content" role="main">
    <h1><?php the_title(); ?></h1>
    <div class="post" id="post-<?php the_ID(); ?>">
      <?php if (have_posts()) : while (have_posts()) : the_post();
        echo '<div class="text_intro">';
          the_content();
        echo '</div>';
        echo '<div class="gallery">'. $pix . '</div>';
      endwhile; endif; ?>
      <div class="jig"></div>
    </div>
  <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
  </div>
<?php get_footer(); ?>
