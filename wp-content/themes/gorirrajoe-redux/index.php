<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>

<div id="content" role="main">
  <?php if (have_posts()) :?>
    <?php if (get_previous_posts_link()) { ?>
      <div class="navigation">
        <div><?php previous_posts_link('Newer Entries &raquo;') ?></div>
      </div>
    <?php } ?>
    <?php while (have_posts()) :the_post(); ?>
      <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
        <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
        <div class="postdate"><?php the_time('F jS, Y') ?> | <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></div>
        <div class="entry">
          <?php if(in_category('photography')) {
            the_content('Read the rest of this entry &raquo;');
            // echo '<a class="more-link" href="'.  get_permalink() .'">Read the rest of this entry &raquo;</a>';
          } else {
            the_content('Read the rest of this entry &raquo;');
          } ?>
        </div>
      </div>
    <?php endwhile; ?>
    <?php if (get_next_posts_link()) { ?>
      <div class="navigation">
        <div><?php next_posts_link('&laquo; Older Entries') ?></div>
      </div>
    <?php } ?>
  <?php else :?>
    <h2 class="center">Not Found</h2>
    <p class="center">Sorry, but you are looking for something that isn't here.</p>
    <?php get_search_form(); ?>
  <?php endif; ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
