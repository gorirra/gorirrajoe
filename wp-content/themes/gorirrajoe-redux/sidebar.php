<?php
	/**
	 * @package WordPress
	 * @subpackage Default_Theme
	 */
	global $wpdb;
?>
	<div id="sidebar" role="complementary">
		<div id="menu"></div>

		<div class="to_top"><a href="#top"><span class="icon-up-open"></span></a></div>

		<ul class="widget_container">

			<li class="widget">
				<h3>Pages</h3>

				<?php wp_nav_menu( array( 'container' => false ) ); ?>

			</li>

			<li class="widget">

				<h3 class="widgettitle">Archives</h3>
				<ul class="archives">

					<?php
						$years = $wpdb->get_col(
							"SELECT DISTINCT YEAR( post_date ) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY post_date DESC"
						);

						foreach( $years as $year ) { ?>

							<li class="archives__year"><a href="<?php echo get_year_link( $year ); ?> "><?php echo $year; ?></a>

								<ul class="archives__monthslist">
									<?php
										$months = $wpdb->get_col(
											"SELECT DISTINCT MONTH( post_date ) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' AND YEAR( post_date ) = '".$year."' ORDER BY post_date DESC"
										);

										foreach( $months as $month ) { ?>

											<li class="archives__month">
												<a href="<?php echo get_month_link( $year, $month ); ?>"><?php echo date( 'F', mktime( 0, 0, 0, $month ) ); ?></a>
											</li>

										<?php }
									?>
								</ul>
							</li>

						<?php }
					?>
				</ul>
			</li>

			<li class="widget">
				<h3 class="widgettitle">Categories</h3>

				<ul>
					<?php
						$args = array(
							'title_li'	=> '',
							'depth'		=> 1
						);
						wp_list_categories( $args );
					?>
				</ul>
			</li>

		</ul>

		<div class="jig"></div>

	</div>

