<?php
	require( 'inc/metadata/gorirrajoe-metadata-galleries.php' );
	require( 'inc/metadata/gorirrajoe-metadata-resumee.php' );
	require( 'inc/metadata/gorirrajoe-metadata-portfolio.php' );

	require( 'inc/post-types/gorirrajoe-posttype-sketches.php' );
	require( 'inc/post-types/gorirrajoe-posttype-portfolio.php' );

	add_action( 'init', array( 'GorirraJoe_Metadata_Galleries', 'singleton' ) );
	add_action( 'init', array( 'GorirraJoe_Metadata_Resumee', 'singleton' ) );
	add_action( 'init', array( 'GorirraJoe_Metadata_Portfolio', 'singleton' ) );

	add_action( 'init', array( 'GorirraJoe_CPT_Sketches', 'singleton' ) );
	add_action( 'init', array( 'GorirraJoe_CPT_Portfolio', 'singleton' ) );


	if ( ! function_exists( 'gorirrajoe_setup' ) ) {

		function gorirrajoe_setup() {

			add_theme_support( 'title-tag' );
			add_theme_support( 'post-thumbnails' );
			add_theme_support( 'html5', array(
				'gallery',
				'caption',
			) );
		}
	}
	add_action( 'after_setup_theme', 'gorirrajoe_setup' );


	function gorirrajoe_enqueue_scripts() {
		// wp_enqueue_script( 'jquery' );
		// wp_enqueue_script( 'jquery-ui-accordion', array( 'jquery' ) );
		wp_enqueue_style( 'gorirrajoe-style', get_bloginfo( 'stylesheet_directory' ) . '/style.min.css', array(), filemtime( get_template_directory() . '/style.min.css' ) );

		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', '//code.jquery.com/jquery-1.11.2.min.js', array(), false, true );
		wp_enqueue_script( 'jquery' );
		wp_deregister_script( 'jquery-ui-core' );
		wp_register_script( 'jquery-ui-core', '//code.jquery.com/ui/1.11.4/jquery-ui.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'jquery-ui-core' );

		wp_enqueue_script( 'gorirrajoe-main', get_template_directory_uri() . '/javascript/main.min.js', array(), $version, true );

		// portfolio stuff
		if( get_post_type() === 'portfolio' || is_page_template( 'resume.php' ) ) {
			wp_enqueue_style( 'portfolio-style', get_template_directory_uri() . '/css/portfolio.min.css', array(), filemtime( get_template_directory() . '/css/portfolio.min.css' ) );
			wp_enqueue_script( 'portfolio-script', get_template_directory_uri() . '/javascript/portfolio-main.min.js', array(), filemtime( get_template_directory() . '/javascript/portfolio-main.min.js' ), true );
		}


	}
	add_action ( 'wp_enqueue_scripts', 'gorirrajoe_enqueue_scripts' );


	/**
	 * Adding Google fonts asynchronously because it blocks page loading if loaded old fashion way
	 */
	function add_fonts_asynchronously() { ?>

		<script>
			WebFontConfig = {
				classes: false,
				events: false,
				google: {
					families: ['Cabin:700', 'Open+Sans:400,700']
				},
			};

			(function(d) {
				var wf = document.createElement('script');
				wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
					'://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'true';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			})(document);
		</script>

		<?php
	}
	add_action( 'wp_head', 'add_fonts_asynchronously', 0 );


	function gorirrajoe_widgets_init() {
		register_sidebar( array(
			'name'          => 'Sidebar',
			'id'            => 'sidebar-1',
			'description'   => '',
			'before_widget'	=> '<li id="%1$s" class="widget %2$s">',
			'after_widget'	=> '</li>',
			'before_title'	=> '<h3 class="widgettitle">',
			'after_title'	=> '</h3>',
		) );
	}
	add_action( 'widgets_init', 'gorirrajoe_widgets_init' );


	function get_search_form2( $echo = true ) {
		do_action( 'get_search_form' );

		$search_form_template = locate_template( 'searchform.php' );

		if ( '' != $search_form_template ) {
			require( $search_form_template );
			return;
		}

		$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
			<div>
				<label class="screen-reader-text" for="s">' . __( '' ) . '</label>
				<input type="text" value="' . get_search_query() . '" name="s" id="s" />
			</div>
		</form>';

		if ( $echo ) {
			echo apply_filters( 'get_search_form', $form );
		} else {
			return apply_filters( 'get_search_form', $form );
		}
	}

	// remove nasty features of WP
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
