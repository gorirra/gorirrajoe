jQuery(document).ready(function($) {
	// search toggle
	var $searchToggle = $('#search_toggle');

	$searchToggle.on('click', function(e) {
		e.preventDefault();

		$('.hidden_search').slideToggle();
	});


	// accordions
	$( "#accordion" ).accordion({
		header: "div",
		heightStyle: "content"
	});

	$( "#accordion2" ).accordion({
		header: "div",
		heightStyle: "content"
	});


	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 750);
				return false;
			}
		}
	});


	// sidebar archives toggle
	$( '.archives__year a' ).on('click', function(e) {
		var $monthsList = $( '.archives__monthslist' );

		e.preventDefault();

		$monthsList.each( function() {
			$( this ).slideUp();
		});


		if( $( this ).next().is( ':visible' ) )  {
			$( this ).next().slideUp();
		} else {
			$( this ).next().slideToggle();
		}

	});
});