jQuery(document).ready(function($) {
	// owl carousels
	var $owl = $( '.owl-carousel' ),
		$portfolioItem = $( '.portfolio__tabs-item' ),
		$portfolioContent = $( '.portfolio__tabs-content' );

	if( $owl.length > 0 ) {
		$owl.owlCarousel({
			'items' : 1,
			'nav' : true,
			'lazyLoad' : true,
			'center' : true,
			'margin' : 30,
			'navText' : ['<span class="icon-left-open">','<span class="icon-right-open">']
		});
	}


	// default show portfolio (first load)
	$( '[data-content="portfolio"]' ).css( 'display', 'block' );


	// tabs
	$portfolioItem.on( 'click', function( e ) {
		e.preventDefault();

		var $datatrigger = $( this ).data( 'trigger' );

		$portfolioItem.each( function() {
			$( this ).removeClass( 'portfolio__tabs-active' );
		});

		$( this ).addClass( 'portfolio__tabs-active' );
		$( '.portfolio__tabs-content' ).css( 'display', 'none' );
		$( '[data-content='+ $datatrigger +']' ).css( 'display', 'block' );
	});



});

// fancybox
$("[data-fancybox]").fancybox();