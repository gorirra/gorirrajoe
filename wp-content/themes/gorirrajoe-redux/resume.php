<?php
	/*
		Template Name: Resume
	*/

	$resume			= get_post_meta( get_the_ID(), '_resumee', 1 );
	$city_state_zip	= get_post_meta( get_the_ID(), '_city_state_zip', 1 );
	$phone			= get_post_meta( get_the_ID(), '_phone', 1 );
	$email			= get_post_meta( get_the_ID(), '_email', 1 );

	get_header( 'resumee' );
?>

<div id="banner">

	<div id="nameAndLoc">
		<h3 id="bigName">Joseph Hernandez</h3>
		<ul class="location">
			<li><?php echo $city_state_zip; ?></li>
			<li><?php echo $phone; ?></li>
			<li><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></li>

			<?php if( $resume != '' ) {
				echo '<li><a href="'. $resume .'" target="_blank"><img class="download_pdf" src="'.get_bloginfo( 'stylesheet_directory' ).'/images/pdf_icon.gif" alt="Download Résumé as PDF">Download Résumé as PDF</a></li>';
			} ?>

		</ul>
	</div>

</div>


<div id="contentResume" role="main">

	<?php if( have_posts() ) :while( have_posts() ) : the_post(); ?>

		<div class="post" id="post-<?php the_ID(); ?>">

			<div class="entry">

				<ul class="portfolio__tabs">
					<li><a class="portfolio__tabs-item portfolio__tabs-active" href="#" data-trigger="portfolio">Portfolio</a></li>
					<li><a class="portfolio__tabs-item" href="#" data-trigger="resume">Résumé</a></li>
				</ul>

				<?php
					/**
					 * portfolio content
					 */
					if ( false === ( $portfolio_query = get_transient( 'portfolio_query_results' ) ) ) {
						$args = array(
							'posts_per_page'	=> -1,
							'post_type'			=> 'portfolio',
						);
						$portfolio_query = new WP_Query( $args );
						set_transient( 'portfolio_query_results', $portfolio_query, 60 * MINUTE_IN_SECONDS );
					}

					if( $portfolio_query->have_posts() ) {

						echo '<div class="portfolio__tabs-content" data-content="portfolio">
							<ul class="portfolio">';

								while( $portfolio_query->have_posts() ) {
									$portfolio_query->the_post();

									$portfolio_url = get_post_meta( get_the_ID(), '_gj_url', 1 ) != '' ? get_post_meta( get_the_ID(), '_gj_url', 1 ) : get_the_permalink() ;

									echo '<li class="portfolio__item">
										<a href="'. $portfolio_url .'">
											<img src="'. get_the_post_thumbnail_url( get_the_ID(), 'medium' ) .'" alt="'. get_the_title() .'">
											<p>'. get_the_title() .'</p>
										</a>
									</li>';

								}

							echo '</ul>
						</div>';

						wp_reset_postdata();

					}


					echo '<div class="portfolio__tabs-content" data-content="resume">';

						the_content();

					echo '</div>';
				?>

			</div>

		</div>

	<?php endwhile; endif; ?>

</div>



<?php get_footer();