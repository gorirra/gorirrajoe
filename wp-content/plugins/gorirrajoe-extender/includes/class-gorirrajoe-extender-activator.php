<?php

/**
 * Fired during plugin activation
 *
 * @link       https://gorirrajoe.com
 * @since      1.0.0
 *
 * @package    Gorirrajoe_Extender
 * @subpackage Gorirrajoe_Extender/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gorirrajoe_Extender
 * @subpackage Gorirrajoe_Extender/includes
 * @author     Joey Hernandez <joey@gorirra.com>
 */
class Gorirrajoe_Extender_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
