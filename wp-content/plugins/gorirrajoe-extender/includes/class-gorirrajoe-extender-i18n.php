<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://gorirrajoe.com
 * @since      1.0.0
 *
 * @package    Gorirrajoe_Extender
 * @subpackage Gorirrajoe_Extender/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Gorirrajoe_Extender
 * @subpackage Gorirrajoe_Extender/includes
 * @author     Joey Hernandez <joey@gorirra.com>
 */
class Gorirrajoe_Extender_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'gorirrajoe-extender',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
