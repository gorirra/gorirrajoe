<?php
class Gorirrajoe_Extender_Theme_Includes
{
    public static function load_toolkit_partials()
    {
        // global site theme options
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/gorirrajoe-theme-options.php';

        /**
         * post types
         */
        // sketches
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/post-types/gorirrajoe-post-type-sketches.php';
        add_action('init', array('Gorirrajoe_Extender_CPT_Sketches', 'singleton'));

        // portfolio
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/post-types/gorirrajoe-post-type-portfolio.php';
        add_action('init', array('Gorirrajoe_Extender_CPT_Portfolio', 'singleton'));

        /**
         * taxonomies
         */
        // require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/taxonomies/gorirrajoe-taxonomy-poi-types.php';
        // add_action('init', array('Gorirrajoe_Extender_Taxonomy_POI_Type', 'singleton'));

        /**
         * metadata
         */
        // resumee
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/metadata/gorirrajoe-metadata-resumee.php';
        add_action('init', array('Gorirrajoe_Extender_Metadata_Resumee', 'singleton'));

        // galleries
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/metadata/gorirrajoe-metadata-galleries.php';
        add_action('init', array('Gorirrajoe_Extender_Metadata_Galleries', 'singleton'));
    }
}
