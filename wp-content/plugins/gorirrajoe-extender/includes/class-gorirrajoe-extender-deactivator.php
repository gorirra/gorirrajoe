<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://gorirrajoe.com
 * @since      1.0.0
 *
 * @package    Gorirrajoe_Extender
 * @subpackage Gorirrajoe_Extender/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gorirrajoe_Extender
 * @subpackage Gorirrajoe_Extender/includes
 * @author     Joey Hernandez <joey@gorirra.com>
 */
class Gorirrajoe_Extender_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
