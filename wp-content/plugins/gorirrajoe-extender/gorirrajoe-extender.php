<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://gorirrajoe.com
 * @since             1.0.0
 * @package           Gorirrajoe_Extender
 *
 * @wordpress-plugin
 * Plugin Name:       gorirrajoe Theme Extender
 * Plugin URI:        https://gorirrajoe.com
 * Description:       Adds several useful features to the theme, including: Post Types, Taxonomies, Meta options for templates and page types, and Global theme options.
 * Version:           1.0.0
 * Author:            Joey Hernandez
 * Author URI:        https://gorirrajoe.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gorirrajoe-extender
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GORIRRAJOE_EXTENDER_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gorirrajoe-extender-activator.php
 */
function activate_gorirrajoe_extender() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gorirrajoe-extender-activator.php';
	Gorirrajoe_Extender_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gorirrajoe-extender-deactivator.php
 */
function deactivate_gorirrajoe_extender() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gorirrajoe-extender-deactivator.php';
	Gorirrajoe_Extender_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gorirrajoe_extender' );
register_deactivation_hook( __FILE__, 'deactivate_gorirrajoe_extender' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gorirrajoe-extender.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gorirrajoe_extender() {

	$plugin = new Gorirrajoe_Extender();
	$plugin->run();

}
run_gorirrajoe_extender();
