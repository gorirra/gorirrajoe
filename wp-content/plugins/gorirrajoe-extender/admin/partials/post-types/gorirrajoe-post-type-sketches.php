<?php
/**
 * Custom Post Types | Sketches
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */

class Gorirrajoe_Extender_CPT_Sketches
{

    static $instance = false;

    public function __construct()
    {

        $this->gorirrajoe_cpt_sketches();

    }

    /**
     * Register Meta Boxes
     *
     * Defines all the meta boxes with CMB2 used by this custom post type.
     *
     */
    protected function gorirrajoe_cpt_sketches()
    {

        $labels = array(
            'name'               => _x('Sketches', 'post type general name'),
            'singular_name'      => _x('Sketch', 'post type singular name'),
            'add_new'            => _x('Add New Sketch', 'Sweet'),
            'add_new_item'       => __('Add New Sketch'),
            'edit_item'          => __('Edit Sketch'),
            'new_item'           => __('New Sketch'),
            'view_item'          => __('View Sketch'),
            'search_items'       => __('Search Sketches'),
            'not_found'          => __('No Sketches found'),
            'not_found_in_trash' => __('No Sketches found in Trash'),
            'parent_item_colon'  => '',
        );

        $supports = array('title', 'editor', 'revisions', 'thumbnail', 'comments');

        register_post_type('sketches',
            array(
                'labels'      => $labels,
                'public'      => true,
                'supports'    => $supports,
                'menu_icon'   => 'dashicons-format-image',
                'has_archive' => true,
            )
        );
    }

    /**
     * Singleton
     *
     * Returns a single instance of the current class.
     */
    public static function singleton()
    {

        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

}
