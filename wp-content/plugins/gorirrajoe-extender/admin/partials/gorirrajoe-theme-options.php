<?php
/*
 * This snippet has been updated to reflect the official supporting of options pages by CMB2
 * in version 2.2.5.
 *
 * If you are using the old version of the options-page registration,
 * it is recommended you swtich to this method.
 */
add_action('cmb2_admin_init', 'gorirrajoe_register_theme_options_metabox');

/*
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function gorirrajoe_register_theme_options_metabox()
{
    /*
     * Registers options page menu item and form.
     */
    $cmb_options = new_cmb2_box(array(
        'id'           => 'gorirrajoe_option_metabox',
        'title'        => esc_html__('Site Options', 'gorirrajoe'),
        'object_types' => array('options-page'),
        'option_key'   => 'gorirrajoe_options', // The option key and admin menu page slug.
        'icon_url'     => 'dashicons-forms',
    ));

    /*
     * Options fields ids only need
     * to be unique within this box.
     * Prefix is not needed.
     */
    $cmb_options->add_field(array(
        'name' => __('Social URLs', 'gorirrajoe'),
        'id'   => 'social_title',
        'type' => 'title',
    ));
    $cmb_options->add_field(array(
        'name' => 'Twitter',
        'id'   => 'twitter_url',
        'type' => 'text_url',
    ));
    $cmb_options->add_field(array(
        'name' => 'Instagram',
        'id'   => 'instagram_url',
        'type' => 'text_url',
    ));
    $cmb_options->add_field(array(
        'name' => 'LinkedIn',
        'id'   => 'linkedin_url',
        'type' => 'text_url',
    ));

    $cmb_options->add_field(array(
        'name' => __('Default Thumbnail', 'gorirrajoe'),
        'id'   => 'thumbnail_title',
        'type' => 'title',
    ));
    $cmb_options->add_field(array(
        'name' => __('Default Thumbnail', 'gorirrajoe'),
        'id'   => 'default_thumbnail',
        'type' => 'file',
    ));
}

/*
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string $key     Options array key
 * @param  mixed  $default Optional default value
 * @return mixed           Option value
 */
function gorirrajoe_get_option($key = '', $default = false)
{
    if (function_exists('cmb2_get_option')) {
        // Use cmb2_get_option as it passes through some key filters.
        return cmb2_get_option('gorirrajoe_options', $key, $default);
    }

    // Fallback to get_option if CMB2 is not loaded yet.
    $opts = get_option('gorirrajoe_options', $default);

    $val = $default;

    if ('all' == $key) {
        $val = $opts;
    } elseif (is_array($opts) && array_key_exists($key, $opts) && false !== $opts[$key]) {
        $val = $opts[$key];
    }

    return $val;
}
