<?php
/**
 * Custom Metaboxes | Resumee
 */
class Gorirrajoe_Extender_Metadata_Resumee
{

    static $instance = false;

    public function __construct()
    {

        $this->_add_actions();

    }

    public function gorirrajoe_metadata_resumee()
    {

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'           => 'resume',
            'title'        => __('Resume Options', 'vn'),
            'object_types' => array('page'), // Post type
            'context'      => 'side',
            'priority'     => 'high',
            'show_on'      => array('key' => 'page-template', 'value' => 'page-templates/template-resumee.php'),
            'show_names'   => true, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'       => 'Resumee (PDF)',
            'desc'       => 'Upload PDF.',
            'id'         => '_resumee',
            'type'       => 'file',
            'options'    => array(
                'url' => false, // Hide the text input for the url
            ),
            'text'       => array(
                'add_upload_file_text' => 'Add File', // Change upload button text. Default: "Add or Upload File"
            ),
            'query_args' => array(
                'type' => 'application/pdf', // Make library only display PDFs.
            ),
        ));

        $cmb->add_field(array(
            'name' => 'City, State, Zip',
            'id'   => '_city_state_zip',
            'type' => 'text',
        ));

        $cmb->add_field(array(
            'name' => 'Phone',
            'id'   => '_phone',
            'type' => 'text',
        ));

        $cmb->add_field(array(
            'name' => 'Email',
            'id'   => '_email',
            'type' => 'text',
        ));

    }

    /**
     * Singleton
     *
     * Returns a single instance of the current class.
     */
    public static function singleton()
    {

        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Add Actions
     *
     * Defines all the WordPress actions and filters used by this class.
     */
    protected function _add_actions()
    {
        add_action('cmb2_admin_init', array($this, 'gorirrajoe_metadata_resumee'));
    }
}
