<?php
/**
 * Custom Metaboxes | Galleries
 */
class Gorirrajoe_Extender_Metadata_Galleries
{

    static $instance = false;

    public function __construct()
    {

        $this->_add_actions();

    }

    public function gorirrajoe_metadata_galleries()
    {

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'           => 'gallery',
            'title'        => __('Gallery Pix', 'gorirrajoe'),
            'object_types' => array('post'), // Post type
            'context'      => 'side',
            'priority'     => 'high',
            'show_names'   => true, // Show field names on the left
        ));

        $cmb->add_field(array(
            'name'    => 'Gallery Pix',
            'desc'    => 'only photos & captions in this area',
            'id'      => '_pix',
            'type'    => 'wysiwyg',
            'options' => array(),
        ));

    }

    /**
     * Singleton
     *
     * Returns a single instance of the current class.
     */
    public static function singleton()
    {

        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Add Actions
     *
     * Defines all the WordPress actions and filters used by this class.
     */
    protected function _add_actions()
    {
        add_action('cmb2_admin_init', array($this, 'gorirrajoe_metadata_galleries'));
    }
}
