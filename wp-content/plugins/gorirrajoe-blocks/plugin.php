<?php
/**
 * Plugin Name: gorirrajoe - Gutenberg Blocks
 * Plugin URI: https://github.com/ahmadawais/create-guten-block/
 * Description: A powerful plugin that allows a content editor the ability to add various modules to a page with ease.
 * Author: Joey Hernandez
 * Author URI: https://gorirrajoe.com
 * Version: 1.0.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package GJML
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path(__FILE__) . 'src/init.php';
