/**
 * gorirrajoeBlox - Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

//  shared css
import './_shared/style-imports.js';

//  partials
// import './partials/two-call-to-action/block.js';
// import './partials/one-call-to-action/block.js';
// import './partials/three-column-content/block.js';
// import './partials/three-column-content/block-part.js';
// import './partials/form-fields/input.js';
// import './partials/form-fields/textarea.js';
// import './partials/form-fields/dropdown.js';
// import './alternating-featured-content/block-part.js';
// import './accordion-module/block-part.js';
// import './clients-carousel/block-part.js';
// import './testimonials-carousel/block-part.js';

// import './static-image-hero/block.js';
// import './call-to-action-centered/block.js';
// import './modal/block.js';
// import './full-bleed-featured-content/block.js';
import './recipe/block.js';
// import './two-col-call-to-action/block.js';
// import './video-module/block.js';
// import './alternating-featured-content/block.js';
// import './accordion-module/block.js';
// import './content-wrapper/block.js';
// import './clients-carousel/block.js';
// import './testimonials-carousel/block.js';
// import './three-col-featured-content/block.js';
// import './off-canvas-form/block.js';
// import './form-slim/block.js';
// import './newsletter-signup/block.js';

// dynamic
// import './featured-posts-listing/block.js';
// import './repeating-featured-cards/generic/block.js';
// import './repeating-featured-cards/blog-landing/block.js';
// import './explore-map/block.js';
