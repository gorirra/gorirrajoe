<?php
namespace GJML\Recipe;

add_action('plugins_loaded', __NAMESPACE__ . '\register_recipe_block');

function register_recipe_block()
{
    // Only load if Gutenberg is available.
    if (!function_exists('register_block_type')) {
        return;
    }

    // Hook server side rendering into render callback
    // Make sure name matches registerBlockType in ./index.js
    register_block_type('gjml/recipe', array(
        'render_callback' => __NAMESPACE__ . '\render_recipe_block',
    ));
}

function render_recipe_block($attributes, $content)
{
    $intro       = (array_key_exists('introContent', $attributes) && $attributes['introContent'] !== '') ? $attributes['introContent'] : '';
    $ingredients = (array_key_exists('ingredientsList', $attributes) && $attributes['ingredientsList'] !== '') ? $attributes['ingredientsList'] : '';
    $steps       = (array_key_exists('stepsList', $attributes) && $attributes['stepsList'] !== '') ? $attributes['stepsList'] : '';

    ob_start();

    echo '
        <h2>Ingredients</h2>
        <ul>'.
            $ingredients .'
        </ul>
        <h2>Steps</h2>
        <ol>'.
            $steps .'
        </ol>
    ';

    $output = ob_get_contents();

    ob_end_clean();

    return $output;
}
