<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package GJML
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

function gjml_block_categories($categories, $post)
{
    return array_merge(
        $categories,
        array(
            array(
                'slug'  => 'gjml-cat',
                'title' => __('gorirrajoe Blox', 'gjml'),
            ),
        )
    );
}
add_filter('block_categories', 'gjml_block_categories', 10, 2);

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * Assets enqueued:
 * 1. blocks.style.build.css - Frontend + Backend.
 * 2. blocks.build.js - Backend.
 * 3. blocks.editor.build.css - Backend.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function gjml_block_assets()
{ // phpcs:ignore
    // Register block styles for both frontend + backend.
    wp_register_style(
        'gjml-style-css', // Handle.
        plugins_url('dist/blocks.style.build.css', dirname(__FILE__)), // Block style CSS.
        is_admin() ? array('wp-editor') : null, // Dependency to include the CSS after it.
        null// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
    );

    // Register block editor script for backend.
    wp_register_script(
        'gjml-block-js', // Handle.
        plugins_url('/dist/blocks.build.js', dirname(__FILE__)), // Block.build.js: We register the block here. Built with Webpack.
        array('wp-blocks', 'wp-i18n', 'wp-element', 'wp-block-editor', 'wp-components', 'wp-data'), // Dependencies, defined above.
        null, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
        true// Enqueue the script in the footer.
    );

    // Register block editor styles for backend.
    wp_register_style(
        'gjml-block-editor-css', // Handle.
        plugins_url('dist/blocks.editor.build.css', dirname(__FILE__)), // Block editor CSS.
        array('wp-edit-blocks'), // Dependency to include the CSS after it.
        null// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
    );

    // WP Localized globals. Use dynamic PHP stuff in JavaScript via `gjmlGlobal` object.
    wp_localize_script(
        'gjml-block-js',
        'gjmlGlobal', // Array containing dynamic data for a JS Global.
        [
            'pluginDirPath' => plugin_dir_path(__DIR__),
            'pluginDirUrl'  => plugin_dir_url(__DIR__),
            // Add more data here that you want to access from `gjmlGlobal` object.
        ]
    );

    /**
     * Register Gutenberg block on server-side.
     *
     * Register the block on server-side to ensure that the block
     * scripts and styles for both frontend and backend are
     * enqueued when the editor loads.
     *
     * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
     * @since 1.16.0
     */
    register_block_type(
        'gjml/block-gjml-blocks', array(
            // Enqueue blocks.style.build.css on both frontend & backend.
            'style'         => 'my_block-cgb-style-css',
            // Enqueue blocks.build.js in the editor only.
            'editor_script' => 'gjml-block-js',
            // Enqueue blocks.editor.build.css in the editor only.
            'editor_style'  => 'gjml-block-editor-css',
        )
    );
}

// Hook: Block assets.
add_action('init', 'gjml_block_assets');

include plugin_dir_path(__FILE__) . '/recipe/index.php';
// include plugin_dir_path(__FILE__) . '/accordion-module/index-partial.php';
// include plugin_dir_path(__FILE__) . '/alternating-featured-content/index.php';
// include plugin_dir_path(__FILE__) . '/alternating-featured-content/index-partial.php';
// include plugin_dir_path(__FILE__) . '/banner-callout/index.php';
// include plugin_dir_path(__FILE__) . '/call-to-action-centered/index.php';
// include plugin_dir_path(__FILE__) . '/clients-carousel/index.php';
// include plugin_dir_path(__FILE__) . '/clients-carousel/index-partial.php';
// include plugin_dir_path(__FILE__) . '/content-wrapper/index.php';
// include plugin_dir_path(__FILE__) . '/explore-map/index.php';
// include plugin_dir_path(__FILE__) . '/featured-posts-listing/index.php';
// include plugin_dir_path(__FILE__) . '/form-slim/index.php';
// include plugin_dir_path(__FILE__) . '/full-bleed-featured-content/index.php';
// include plugin_dir_path(__FILE__) . '/modal/index.php';
// include plugin_dir_path(__FILE__) . '/newsletter-signup/index.php';
// include plugin_dir_path(__FILE__) . '/off-canvas-form/index.php';
// include plugin_dir_path(__FILE__) . '/repeating-featured-cards/blog-landing/index.php';
// include plugin_dir_path(__FILE__) . '/repeating-featured-cards/generic/index.php';
// include plugin_dir_path(__FILE__) . '/static-image-hero/index.php';
// include plugin_dir_path(__FILE__) . '/testimonials-carousel/index.php';
// include plugin_dir_path(__FILE__) . '/testimonials-carousel/index-partial.php';
// include plugin_dir_path(__FILE__) . '/three-col-featured-content/index.php';
// include plugin_dir_path(__FILE__) . '/two-col-call-to-action/index.php';
// include plugin_dir_path(__FILE__) . '/video-module/index.php';

// load special functions
// require_once plugin_dir_path(__FILE__) . '/off-canvas-form/functions.php';
// require_once plugin_dir_path(__FILE__) . '/explore-map/functions.php';
// require_once plugin_dir_path(__FILE__) . '/newsletter-signup/functions.php';
// require_once plugin_dir_path(__FILE__) . '/repeating-featured-cards/blog-landing/functions.php';
// require_once plugin_dir_path(__FILE__) . '/repeating-featured-cards/generic/functions.php';
