<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'gorirrajoe-mysql' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'W$WJA.E: ~@>Q>sGhY?%7hy$ EmO<~K5sXCvLiD6/2r.}2tA4@+05F+%aLb=S*qi');
define('SECURE_AUTH_KEY', 'z:zNc*/x; H>w~:C4`io+)Fl(0d%}eMRreD&u=+:e8A:UB,3*,Nz16pjLc^.+2N[');
define('LOGGED_IN_KEY', 'ZXVzo`R(+6xq[|9%tdY`L.lzYxHpr=(MM3BQg kzzF3:H.q!YJ[%SiZg@ vZ#W|0');
define('NONCE_KEY', 'wf<V06iAc.^*kd!;|3j+m&Nj|,M$8%DWN[57TYB*--_9pz_4xQS-Y?cfPllQu~/l');
define('AUTH_SALT', '|>6H2HP{PN9Uhglu6z?z3)j<B6g7.+I77-a)%M=]Z8D%P$%HYx1b)18w%c!.c@|)');
define('SECURE_AUTH_SALT', 'k|Yvx|BDHEFlS=FU-<GQ?0uW2]8-`^2>3{l.qPUGzaPXrwZf.873s=A7EHec|6Xc');
define('LOGGED_IN_SALT', '&pQ[DYIJyKs}dXvgXz{gsyVkn.70;nnS_nNBxc%*V-76MD,qg[]ZsFtn;c$wJYnA');
define('NONCE_SALT', 'eLA>$U?EHL!-6c-#N=/iy<7:s-]=}A}&xwq6TE|+k-plM19Tz-854n+<&8/YQ>|H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('FS_METHOD', 'direct');
define('TRANSIENT', 60 * MINUTE_IN_SECONDS);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
