<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'joe_test' );

/** MySQL database username */
define( 'DB_USER', 'c35c05dfacee' );

/** MySQL database password */
define( 'DB_PASSWORD', 'b59b8fa59b61d3fa' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rT^sxdqBvb&zkTkpG4brN):gD(y%`j@}Tqc<|FfG;uQg8NeeQ6sw!To^w|0nNY&U');
define('SECURE_AUTH_KEY',  '|xbxu%Oshy:is;{Wo,xAqQR-MmWB*BSASU(Q#}yK|J?|n,(LR5!9Qn:0ds?; C?&');
define('LOGGED_IN_KEY',    'VkazGS[h7+O9-J9},4.Mw&n]TN3qf*s>eQz3r+/9{|c`f#~P!-&=1P;Qm<6I:4*C');
define('NONCE_KEY',        ']@+<f7F-f$Y5%ZSxm8|hVlk0&7OxJt+H72%HFVNFY;d4K-;Sx>2`m:+*#iV;u/S-');
define('AUTH_SALT',        '{[F{ZSCBt5QgJxey!4?25n/-S!iT}Oi,rmP] K3KHj+.x8rSD<`=ejuX=E6,j*1V');
define('SECURE_AUTH_SALT', 'kgZN:g0N5-u|;bZ4;<rB`M`*A|F#J|bN.h*zPKV+ivs+NN^j/-C) bogW4kq5l+B');
define('LOGGED_IN_SALT',   'vxHc-<O;]w0S=[pQs?obk`Lzp9:NGDUtAyedQX+TsE$li5NAsW:>yw#^~cjlsXpy');
define('NONCE_SALT',       'Xkfr-]= [)J;c-N=wO5:qE|H)@B~~B5Jbp^oY<`*IkZ3I*b+vg!Z:dh-x!qu-0Vo');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);
define('FS_METHOD', 'direct');
define('TRANSIENT', 60);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
