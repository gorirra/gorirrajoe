# README #

This README contains steps necessary to get your application up and running.

## System Requirments ##

* Visual Studio Code w/ Docker Plugin
* Docker Desktop (https://www.docker.com/products/docker-desktop)

## What is this repository for? ##

* Docker container for local dev of gorirrajoe
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Steps to get Docker instace running ##
1. You will need access to the rdi-module-library repository on Bitbucket:

    1. Clone down the rdi-module-library repository:

        ```$ git clone git@bitbucket.org:gorirra/gorirrajoe.git```

    2. MySQL database dump is included in the repo
        1. docker/configs/local/initdb.d

2. Edit the docker/configs/local/15-xdebug.ini file.
    1. Get your machines IP address and set the IP for the setting xdebug.remote_host into the 15-xdebug.ini file. You can use ipconfig command from your windows host to see the IP of "Ethernet adapter Local Area Connection"

        To get IP address in windows command prompt run:

        ```$ ipconfig /all```

        and locate the Ethernet adapter Ethernet section value "IPv4 address"

3. Execute the following as administrator:

	```$ docker-compose up -d --build --force-recreate```

    or

	```$ docker-compose build --force-rm```

    which will build, force remove intermediate containers, and startup the containers. You only do this to rebuild the docker image.

1. Once you've built the docker images, you can bring up the container using:

	```$ docker-compose up```

1. Open browser and access site:
	- http://gorirrajoe.loc/ (for HTTP access)
    - https://gorirrajoe.loc/ (for HTTPS access)

    NOTE:
	- Make sure no other services are binding to ports 80 and 443
	  such as IIS otherwise you will receive an error when running "docker-compose up"

## xDebug in VSCode ##

Install the PHP Debug vscode extension

- https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug

1. Install PHP 7 and XDebug onto your local machine for the correct version of PHP (7)
    - Install PHP 5.6 through IIS Management Studio (may also include Microsoft Drivers 3.2 for PHP v5.6 for SQL Server in IIS, Windows Cache Extension 1.3 for PHP 5.6, PHP 5.6.31, PHP Manager for IIS)
        - https://xdebug.org/download.php

1. Allow inbound traffic to port 9000 on host machine

## Tips ##

To have the docker instance correctly handle internal rml requests (ex. cURL), edit the /etc/hosts file in the docker instance and add the following line:

    127.0.0.1   gorirrajoe.loc

## Troubleshooting Database Error of Missing Tables ##
You may get a database error when accessing the site for the first time. This may be due to
the database not being fully imported. You will have to import the database dump file from
command line.

1. Edit the mysql dump file and put the following line at the top:

    ```SET sql_mode='';```

1. Shell into the rml-mysql docker container. You may do this by using the Docker plugin in VSCode.

1. Within your shell session, import the database from the command line:

    ```$ mysql -u wordpress -pwordpress wordpress < /docker-entrypoint-initdb.d/[MYSQL DUMP FILENAME]```

## Exporting Local Database ##
In the `/gorirrajoe/docker/configs/local` folder:

```$ docker exec gorirrajoe-mysql mysqldump -uroot -pwordpress wordpress > initdb.d/db-backup.sql```

